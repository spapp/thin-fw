<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.18.
 */

namespace Thin;

use Thin\Traits\Configurable;
use Thin\Traits\ConfigurableInterface;

/**
 * Class Template
 *
 * @example config
 *
 * array (
 *      'path'           => '',
 *      'suffix'         => 'phtml',
 *      'template'       => '',
 *      'minimize'       => true,
 *      'withoutComment' => true
 * );
 */
class Template implements ConfigurableInterface {

    use Configurable;

    const PATTERN_COMMENT  = '~<!--(.|\s)*?-->~';
    const PATTERN_MINIMIZE = '~(?>[^\S ]\s*|\s{2,})(?=(?:(?:[^<]++|<(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b|\z))~ix';

    const CONFIG_KEY        = 'template';
    const DEFAULT_PATH_NAME = 'template';

    /**
     * @var Config
     */
    protected $params = null;
    /**
     * @var string
     */
    protected $path = null;
    /**
     * @var string
     */
    protected $suffix = 'phtml';

    /**
     * @var string
     */
    protected $template = null;
    /**
     * @var bool
     */
    protected $minimize = false;
    /**
     * @var bool
     */
    protected $withoutComment = true;
    /**
     * @var array
     * @static
     */
    protected static $defaults = array(
            'path'           => null,
            'suffix'         => 'phtml',
            'template'       => null,
            'minimize'       => true,
            'withoutComment' => true
    );

    /**
     * Constructor
     *
     * @param Config|array|string $aConfig
     */
    public function __construct($aConfig) {
        if (is_string($aConfig)) {
            $aConfig = array(
                    'template' => $aConfig
            );
        }

        $this->setConfig(self::$defaults);
        $this->setConfig($aConfig);

        $this->params = new Config(array());
    }

    /**
     * Sets template defaults configs
     *
     * @param array $aDefaultsConfig
     *
     * @static
     * @return void
     */
    public static function setDefaults(array $aDefaultsConfig) {
        self::$defaults = array_merge(self::$defaults, $aDefaultsConfig);
    }

    /**
     * Returns a new Template instance
     *
     * @param string $aTemplate
     *
     * @static
     * @return Template
     */
    public static function create($aTemplate = '') {
        return new self($aTemplate);
    }

    /**
     * Returns a template variable
     *
     * If the $aEscape is TRUE then converts all applicable characters to HTML entities
     *
     * @param string $aName
     * @param mixed  $aDefault
     * @param bool   $aEscape
     *
     * @return mixed
     */
    public function get($aName, $aDefault = null, $aEscape = false) {
        $value = $this->params->get($aName, $aDefault);

        if (true === $aEscape) {
            $value = htmlentities($value, ENT_COMPAT);
        }

        return $value;
    }

    /**
     * Sets template variable
     *
     * @param string $aName
     * @param mixed  $aValue
     *
     * @return $this
     *
     * @return $this
     * @throws \Thin\Application\ErrorException
     */
    public function set($aName, $aValue) {
        $this->params->set($aName, $aValue);

        return $this;
    }

    /**
     * Returns TRUE if the template variavble is exists
     *
     * @param string $aName
     *
     * @return bool
     */
    public function has($aName) {
        return $this->params->has($aName);
    }

    /**
     * Returns TRUE if the compression is turned on
     *
     * @return boolean
     */
    public function isMinimize() {
        return (bool)$this->minimize;
    }

    /**
     * Sets the output to be minimized or not
     *
     * @param boolean $aMinimize
     *
     * @return $this
     */
    public function setMinimize($aMinimize) {
        $this->minimize = $aMinimize;

        return $this;
    }

    /**
     * Returns TRUE if the comments removing is turned on
     *
     * @return boolean
     */
    public function isWithoutComment() {
        return (bool)$this->withoutComment;
    }

    /**
     * Set to TRUE if you want to remove comments from the output
     *
     * @param boolean $aWithoutComment
     *
     * @return $this
     */
    public function setWithoutComment($aWithoutComment) {
        $this->withoutComment = $aWithoutComment;

        return $this;
    }

    /**
     * Returns template name
     *
     * @return string
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Sets current template
     *
     * @param string $aTemplate
     *
     * @return $this
     */
    public function setTemplate($aTemplate) {
        $this->template = $aTemplate;

        return $this;
    }

    /**
     * Returns template suffix
     *
     * @return string
     */
    public function getSuffix() {
        return $this->suffix;
    }

    /**
     * Sets template suffix
     *
     * @param string $aSuffix
     *
     * @return $this
     */
    public function setSuffix($aSuffix) {
        $this->suffix = $aSuffix;

        return $this;
    }

    /**
     * Returns template base path
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Sets template base path
     *
     * @param string $aPath
     *
     * @return $this
     */
    public function setPath($aPath) {
        $this->path = $aPath;

        return $this;
    }

    /**
     * Appends template params
     *
     * @param array $aParams
     *
     * @return $this
     */
    public function addParams(array $aParams) {
        foreach ($aParams as $name => $value) {
            $this->params->set($name, $value);
        }

        return $this;
    }

    /**
     * Magice method
     *
     * @see Template::render()
     *
     * @return string
     */
    public function __toString() {
        return $this->render();
    }

    /**
     * Returns a rendered template as string
     *
     * @param null|string $aTemplateFile
     *
     * @return string
     */
    public function render($aTemplateFile = null) {
        if (null === $aTemplateFile) {
            $aTemplateFile = $this->getTemplate();
        }

        ob_start();

        include $this->getTemplateFullPath($aTemplateFile);

        return $this->filter(ob_get_clean());
    }

    /**
     * Returns filtered template text
     *
     * @param string $aText
     *
     * @return string
     */
    protected function filter($aText) {
        if (true === $this->isWithoutComment()) {
            $aText = preg_replace(self::PATTERN_COMMENT, '', $aText);
        }

        if (true === $this->isMinimize()) {
            $aText = preg_replace(self::PATTERN_MINIMIZE, ' ', $aText);
        }

        return $aText;
    }

    /**
     * Returns the absolute path of template
     *
     * @param string $aTemplateFile
     *
     * @return string
     */
    protected function getTemplateFullPath($aTemplateFile) {
        if (preg_match('~^/~', $aTemplateFile)) {
            return $aTemplateFile;
        }

        $suffix = $this->getSuffix();
        $path   = $this->getPath();

        if (!preg_match('~\.' . $suffix . '$~', $aTemplateFile)) {
            $aTemplateFile .= '.' . $suffix;
        }

        return $path . DIRECTORY_SEPARATOR . $aTemplateFile;
    }
}
