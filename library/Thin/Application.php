<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   test
 * @since     2015.06.17.
 */

namespace Thin;

use Thin\Application\ErrorException;
use Thin\Http\Request;
use Thin\Http\Response;
use Thin\Traits\Configurable;
use Thin\Traits\ConfigurableInterface;
use Thin\Template\Layout;

/**
 * Class Application
 */
abstract class Application implements ConfigurableInterface {

    use Configurable;

    const CONFIG_KEY = 'application';

    const DEFAULT_AUTHENTICATE_METHOD  = 'auth';
    const DEFAULT_ERROR_HANDLER_METHOD = 'error';
    /**
     * @var Config
     */
    protected $config = null;
    /**
     * @var string
     */
    protected $configFileName = null;
    /**
     * @var string
     */
    protected $environment = null;
    /**
     * @var string
     */
    protected $ns = null;
    /**
     * @var bool
     */
    protected $terminated = false;
    /**
     * @var string
     */
    protected $path = null;
    /**
     * @var Router
     */
    protected $router = null;
    /**
     * @var Controller
     */
    protected $controller = null;
    /**
     * @var array
     */
    protected $errorHandler = null;
    /**
     * @var array
     */
    protected $auth = null;

    /**
     * Constructor
     *
     * @param string $aConfigFileName
     * @param string $aEnvironment
     *
     * @throws \Exception
     */
    final public function __construct($aConfigFileName, $aEnvironment) {
        $this->environment    = $aEnvironment;
        $this->configFileName = $aConfigFileName;

        set_error_handler(
                array(
                        $this,
                        'handleError'
                )
        );

        $this->setConfig($this->getConfig()->get(self::CONFIG_KEY, array()));

        $this->bootstrap();
    }

    /**
     * Adjusts application templates default configs
     *
     * @param array $aConfig
     *
     * @return $this
     */
    final public function setTemplate(array $aConfig) {
        if (!isset($aConfig['path'])) {
            $aConfig['path'] = $this->getPath() . '/' . Template::DEFAULT_PATH_NAME;
        }

        Template::setDefaults($aConfig);
        Layout::setDefaults($aConfig);

        return $this;
    }

    /**
     * Adjusts application layout
     *
     * @param array $aConfig
     *
     * @return $this
     */
    final public function setLayout(array $aConfig) {
        $this->getResponse()->setLayout(Layout::create($aConfig));

        return $this;
    }

    /**
     * Adjusts application error handler controller
     *
     * @param array $aConfig
     *
     * @return $this
     */
    final public function setErrorHandler(array $aConfig) {
        try {
            $class = Controller::create(Router::prepareControllerName($aConfig['class']), $this);
        } catch (\Exception $e) {
            $this->handleError($e);
        }

        $method = isset($aConfig['method']) ? $aConfig['method'] : self::DEFAULT_ERROR_HANDLER_METHOD;

        if (isset($class) and method_exists($class, $method)) {
            $this->errorHandler = array(
                    'class'  => $class,
                    'method' => $method
            );
        }

        return $this;
    }

    /**
     * Returns application namespace
     *
     * @return string
     */
    final public function getNs() {
        return $this->ns;
    }

    /**
     * Adjusts application namespace
     *
     * @param string $ns
     *
     * @return $this
     */
    final public function setNs($ns) {
        $this->ns = $ns;

        return $this;
    }

    /**
     * Returns applicaton path
     *
     * This path contains your api implementation
     *
     * @return string
     */
    final public function getPath() {
        return $this->path;
    }

    /**
     * Adjusts applicaton path
     *
     * @param string $path
     *
     * @return $this
     */
    final public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Returns application config
     *
     * @return Config
     */
    final public function getConfig() {
        if (null === $this->config) {
            $this->config = Config::create($this->getConfigFileName(), $this->getEnvironment());
        }

        return $this->config;
    }

    /**
     * Appends some paths to the PHP include_path configuration option
     *
     * @param string|array $aIncludePath
     *
     * @return $this
     */
    final public function setIncludePath($aIncludePath) {
        Autoloader::getInstance()->addIncludePath($aIncludePath);

        return $this;
    }

    /**
     * Returns application config file name
     *
     * @return string
     */
    final public function getConfigFileName() {
        return $this->configFileName;
    }

    /**
     * Returns application environment
     *
     * @return string
     */
    final public function getEnvironment() {
        return $this->environment;
    }

    /**
     * Returns application request
     *
     * @return Request
     */
    final  public function getRequest() {
        return Request::getInstance();
    }

    /**
     * Adjusts autenticate controller and method
     *
     * @param array $aConfig
     *
     * @return $this
     */
    final  public function setAuthenticate(array $aConfig) {
        try {
            $class = Controller::create(Router::prepareControllerName($aConfig['class']), $this);
        } catch (\Exception $e) {
            $this->handleError($e);
        }

        $method = isset($aConfig['method']) ? $aConfig['method'] : self::DEFAULT_AUTHENTICATE_METHOD;

        if (isset($class) and method_exists($class, $method)) {
            $this->auth = array(
                    'class'  => $class,
                    'method' => $method
            );
        }

        return $this;
    }

    /**
     * Returns application response
     *
     * @return Response
     */
    final public function getResponse() {
        return Response::getInstance();
    }

    /**
     * Error handler method
     *
     * If adjusts a custom error handler then run it
     * otherwise the error message will be the response message
     *
     * @param int    $aErrorNo
     * @param string $aErrorMessage
     * @param string $aErrorFile
     * @param int    $aErrorLine
     *
     * @return bool
     */
    final public function handleError($aErrorNo = null, $aErrorMessage = '', $aErrorFile = '', $aErrorLine = 0) {
        if (!isset($aErrorNo)) {
            $error = error_get_last();
            $error = new ErrorException($error['message'], $error['type'], 1, $error['file'], $error['line']);
        } elseif ($aErrorNo instanceof \Exception) {
            $error = $aErrorNo;
        } else {
            $error = new ErrorException($aErrorMessage, $aErrorNo, 1, $aErrorFile, $aErrorLine);
        }

        if (is_array($this->errorHandler)) {
            $this->errorHandler['class']->{$this->errorHandler['method']}($error);
        } else {
            $this->getResponse()->setBody($error->asHtml());
        }

        return true;
    }

    /**
     * Run the application
     *
     * Checks the permissions
     * If sets a custom controller for authenticate
     *
     * @see self::route
     * @see self::terminate
     *
     * @return void
     * @throws \Exception
     */
    final public function run() {
        if (true === $this->authenticate()) {
            $this->route();
        } else {
            $this->getResponse()->setStatusCode(401);
        }

        $this->terminate();
    }

    /**
     * Routes request
     *
     * Creates current controller and executes current method
     *
     * @param string $aControllerName
     * @param string $aMethodName
     * @param array  $aParams
     *
     * @return void
     */
    final public function route($aControllerName = null, $aMethodName = null, array $aParams = array()) {
        if (isset($aControllerName, $aMethodName)) {
            $controllerName = Router::prepareControllerName($aControllerName);
            $methodName     = Router::prepareMethodName($aMethodName);
            $params         = $aParams;
        } else {
            $this->getRouter()->route($this->getRequest());

            $controllerName = $this->getRouter()->getControllerName();
            $methodName     = $this->getRouter()->getMethodName();
            $params         = $this->getRouter()->getMethodParams();
        }

        try {
            $this->controller = Controller::create($controllerName, $this);

            call_user_func_array(
                    array(
                            $this->getController(),
                            $methodName
                    ),
                    $params
            );
        } catch (\Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * Returns current controller
     *
     * @return Controller
     */
    final public function getController() {
        return $this->controller;
    }

    /**
     * Terminates the application
     *
     * Sends current response.
     * If $aExit is equal TRUE then terminate the current script.
     *
     * @param bool|false $aExit
     *
     * @return void
     * @throws \Exception
     */
    final public function terminate($aExit = false) {
        if (true === $this->terminated) {
            throw new ErrorException('The application has been terminated');
        }

        $this->terminated = true;
        $this->getResponse()->send();

        if (true === $aExit) {
            exit(0);
        }
    }

    /**
     * Returns a router instance
     *
     * If it is not exists create it
     *
     * @return Router
     */
    final protected function getRouter() {
        if (null === $this->router) {
            $routerConfig = $this->getConfig()->get('router', array());
            $this->router = Router::create($routerConfig->toArray());
        }

        return $this->router;
    }

    /**
     * Runs the custom authentication logic
     * Otherwise returns TRUE
     *
     * @return boolean
     */
    final protected function authenticate() {
        if (is_array($this->auth)) {
            return $this->auth['class']->{$this->auth['method']}();
        }

        return true;
    }

    /**
     * Implementation of application bootstrapping logic
     *
     * Perhaps override
     *
     * @return mixed
     */
    protected function bootstrap() {

    }
}
