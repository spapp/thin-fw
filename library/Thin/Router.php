<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.17.
 */

namespace Thin;

use Thin\Http\Request;
use Thin\Traits\Configurable;
use Thin\Traits\ConfigurableInterface;

/**
 * Class Router
 */
class Router implements ConfigurableInterface {

    use Configurable;

    const SUFFIX_CONTROLLER = 'Controller';
    const SUFFIX_METHOD     = 'Action';

    const KEY_CONTROLLER = 'controller';
    const KEY_METHOD     = 'method';

    const PATH_FIRST_PART  = ':0';
    const PATH_SECOND_PART = ':1';

    const HTTP_METHOD = 'HTTP_METHOD';

    const DEFAULT_CONTROLLER_AND_METHOD_NAME = 'index';

    /**
     * @var string
     */
    protected $controllerName = null;
    /**
     * @var string
     */
    protected $methodName = null;
    /**
     * @var array
     */
    protected $methodParams = array();
    /**
     * @var array
     */
    protected $default = array(
            self::KEY_CONTROLLER => self::PATH_FIRST_PART,
            self::KEY_METHOD     => self::PATH_SECOND_PART
    );
    /**
     * @var array
     */
    protected $routes = array();

    /**
     * Constructor
     */
    public function __construct(array $aConfig) {
        $this->setConfig($aConfig);
    }

    /**
     * Sets default route values
     *
     * @param array $aDefault
     *
     * @return $this
     */
    public function setDefault(array $aDefault) {
        $this->default = array_merge($this->default, $aDefault);

        return $this;
    }

    /**
     * Returns the specified default route value
     *
     * @param string $aName
     *
     * @return string
     */
    public function getDefault($aName) {
        if (isset($this->default[$aName])) {
            return $this->default[$aName];
        }

        return '';
    }

    /**
     * Returns current controller name
     *
     * @return string
     */
    public function getControllerName() {
        return $this->controllerName;
    }

    /**
     * Sets current controller name
     *
     * @param string $controllerName
     *
     * @return $this
     */
    public function setControllerName($controllerName) {
        $this->controllerName = self::prepareControllerName($controllerName);

        return $this;
    }

    /**
     * Returns current controller handler method name
     *
     * @return string
     */
    public function getMethodName() {
        return $this->methodName;
    }

    /**
     * Sets current controller handler method name
     *
     * @param string $methodName
     *
     * @return $this
     */
    public function setMethodName($methodName) {
        $this->methodName = self::prepareMethodName($methodName);

        return $this;
    }

    /**
     * Returns current params of handler method
     *
     * @return array
     */
    public function getMethodParams() {
        if (!is_array($this->methodParams)) {
            $this->methodParams = array();
        }

        return $this->methodParams;
    }

    /**
     * Sets current params of handler method
     *
     * @param array $methodParams
     *
     * @return $this
     */
    public function setMethodParams($methodParams) {
        $this->methodParams = $methodParams;

        return $this;
    }

    /**
     * Returns route settings
     *
     * @return array
     */
    public function getRoutes() {
        return $this->routes;
    }

    /**
     * Sets route settings
     *
     * @param array $aRoutes
     *
     * @return $this
     */
    public function setRoutes(array $aRoutes) {
        $this->routes = $aRoutes;

        return $this;
    }

    /**
     * Provides the information necessary to process the claim
     *
     * Determines the controller name, worker method name and params
     *
     * @example config.ini
     * <code>
     *      router.default.controller = :0  ; default controller name
     *      router.default.method = :1      ; default method name
     *
     *      router.routes.static.pattern = /css/:fileName
     *      router.routes.static.controller = static
     *      router.routes.static.method = css
     *
     *      router.routes.blog-article.pattern = /blog/:year/:month/:day/:title
     *      router.routes.blog-article.method = show
     *
     *      router.routes.blog-list.pattern = /blog
     *      router.routes.blog-list.controller = blog
     * </code>
     *
     * <code>
     *  // $path = '/css/main-x.css';
     *  $controller = new StaticController($app);
     *  $controller->cssAction('main-x.css');
     *
     *  // $path = '/blog/2015/1/15/the-article-title';
     *  $controller = new BlogController($app);
     *  $controller->showAction(2014, 1, 15, 'the-article-title');
     *
     *  // $path = '/blog';
     *  $controller = new BlogController($app);
     *  $controller->indexAction();
     *
     *  // $path = '/rss';
     *  $controller = new RssController($app);
     *  $controller->indexAction();
     *
     *  // $path = '/';
     *  $controller = new IndexController($app);
     *  $controller->indexAction();
     * </code>
     *
     * @param Request $aRequest
     *
     * @return boolean
     */
    public function route(Request $aRequest) {
        $path           = $aRequest->getPath();
        $routes         = $this->getRoutes();
        $controllerName = null;
        $methodName     = null;

        foreach ($routes as $name => &$route) {
            if (isset($routes['pattern'])) {
                $pattern = '~' . preg_replace('~:([\w\.-]+)\+?~', '([\w\.-])+', $routes['pattern']) . '~i';

                if (preg_match($pattern, $path, $matches)) {
                    if (isset($routes['controller'])) {
                        $this->setControllerName($routes['controller']);
                    }

                    if (isset($routes['method'])) {
                        $this->setMethodName($routes['method']);
                    }

                    $this->setMethodParams(array_slice($matches, 1));

                    break;
                }
            }
        }

        if (!$controllerName) {
            $controllerName = $this->getDefaultName(self::KEY_CONTROLLER, $aRequest);
        }

        if (!$methodName) {
            $methodName = $this->getDefaultName(self::KEY_METHOD, $aRequest);
        }

        $this->setControllerName($controllerName);
        $this->setMethodName($methodName);

        return $this;
    }

    /**
     * @param array $aConfig
     *
     * @static
     * @return Router
     */
    public static function create(array $aConfig) {
        return new self($aConfig);
    }

    /**
     * Returns prepared controller name
     *
     * @param string $aName
     *
     * @static
     * @return string
     */
    public static function prepareControllerName($aName) {
        $name = self::prepareName($aName);

        if (!preg_match('~' . self::SUFFIX_CONTROLLER . '$~', $name)) {
            $name .= self::SUFFIX_CONTROLLER;
        }

        return ucfirst($name);
    }

    /**
     * Returns prepared method name
     *
     * @param string $aName
     *
     * @static
     * @return string
     */
    public static function prepareMethodName($aName) {
        $name = self::prepareName($aName);

        if (!preg_match('~' . self::SUFFIX_METHOD . '$~', $name)) {
            $name .= self::SUFFIX_METHOD;
        }

        return lcfirst($name);
    }

    /**
     * Prepares name
     *
     * @param string $aName
     *
     * @return string
     */
    public static function prepareName($aName) {
        $name = preg_replace('~[^a-z0-9-]~i', '_', $aName);
        $name = str_replace(
                array(
                        '-',
                        '_'
                ),
                ' ',
                $name
        );

        return str_replace(' ', '', ucwords($name));
    }

    /**
     * Specifies the default name of the controller or method
     *
     * @param string  $aName
     * @param Request $aRequest
     *
     * @return string
     */
    protected function getDefaultName($aName, Request $aRequest) {
        $defaultName = $this->getDefault($aName);

        if (preg_match('~:([0-9]+)~', $defaultName, $matches)) {
            $pathParts = explode('/', trim($aRequest->getPath(), '/'));

            if (isset($matches, $matches[1], $pathParts[$matches[1]]) and !empty($pathParts[$matches[1]])) {
                $defaultName = $pathParts[$matches[1]];
            } else {
                $defaultName = self::DEFAULT_CONTROLLER_AND_METHOD_NAME;
            }
        } elseif (self::HTTP_METHOD === strtoupper($defaultName)) {
            $defaultName = strtolower($aRequest->getMethod());
        }

        return $defaultName;
    }
}
