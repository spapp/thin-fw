<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.07.01.
 */
namespace Thin\Template;

use Thin\Template;

/**
 * Class Layout
 */
class Layout extends Template {
    const TPL_CSS_LINK = '<link href="%s" type="text/css" rel="stylesheet"%s>';
    const TPL_JS_LINK  = '<script type="application/javascript" src="%s"></script>';
    /**
     * @var array
     * @static
     */
    protected static $defaults = array(
            'path'           => null,
            'suffix'         => 'phtml',
            'template'       => null,
            'minimize'       => true,
            'withoutComment' => true
    );
    /**
     * @var bool
     */
    protected $xhtml = false;
    /**
     * @var string
     */
    protected $title = '';
    /**
     * @var string
     */
    protected $content = '';
    /**
     * @var array
     */
    protected $css = array();
    /**
     * @var array
     */
    protected $js = array();

    /**
     * Returns TRUE if the layout is xhtml
     *
     * @return bool
     */
    public function isXhtml() {
        return $this->xhtml;
    }

    /**
     * Adjusts xhtml layout
     *
     * @param bool $aXhtml
     *
     * @return void
     */
    public function setXhtml($aXhtml) {
        $this->xhtml = (bool)$aXhtml;
    }

    /**
     * Returns layout content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Adjusts layout content
     *
     * @param string $aContent
     *
     * @return $this
     */
    public function setContent($aContent) {
        $this->content = $aContent;

        return $this;
    }

    /**
     * Adjusts site title
     *
     * @param string $aTitle
     *
     * @return $this
     */
    public function setTitle($aTitle) {
        $this->title = $aTitle;

        return $this;
    }

    /**
     * Returns site title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Appends css files
     *
     * @param string|array $aCss
     *
     * @return $this
     */
    public function appendCss($aCss) {
        $css = (array)$aCss;
        $s   = (true === $this->isXhtml() ? '/' : '');

        foreach ($css as $c) {
            array_push($this->css, sprintf(self::TPL_CSS_LINK, $c, $s));
        }

        return $this;
    }

    /**
     * Returns CSS link tags as HTML
     *
     * @return string
     */
    public function getCss() {
        return implode("\n", $this->css);
    }

    /**
     * Appends javascript files
     *
     * @param string|array $aJs
     *
     * @return $this
     */
    public function appendJs($aJs) {
        $js = (array)$aJs;

        foreach ($js as $j) {
            array_push($this->js, sprintf(self::TPL_JS_LINK, $j));
        }

        return $this;
    }

    /**
     * Returns SCRIPT tags as HTML
     *
     * @return string
     */
    public function getJs() {
        return implode("\n", $this->js);
    }

    /**
     * Sets layout defaults configs
     *
     * @param array $aDefaultsConfig
     *
     * @static
     * @return void
     */
    public static function setDefaults(array $aDefaultsConfig) {
        self::$defaults = array_merge(self::$defaults, $aDefaultsConfig);
    }

    /**
     * Returns a new Layout instance
     *
     * @param string $aLayout
     *
     * @static
     * @return Layout
     */
    public static function create($aLayout = '') {
        return new self($aLayout);
    }
}
