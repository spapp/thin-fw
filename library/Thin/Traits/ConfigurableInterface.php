<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package
 * @since     2015.06.17.
 */

namespace Thin\Traits;

interface ConfigurableInterface {
    public function setConfig($aConfig);
}
