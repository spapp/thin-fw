<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package
 * @since     2015.06.17.
 */

namespace Thin\Traits;

use Thin\Config;

trait Configurable {
    /**
     * @param array|Config $aConfig
     *
     * @return mixed
     * @throws \Exception
     */
    public function setConfig($aConfig) {
        if ($aConfig instanceof Config) {
            $aConfig = $aConfig->toArray();
        }

        if (!is_array($aConfig)) {
            throw new \Exception('Not supported type');
        }

        foreach ($aConfig as $name => $value) {
            $setMethod = 'set' . ucfirst($name);
            $appendMethod = 'append' . ucfirst($name);

            if (method_exists($this, $setMethod)) {
                $this->$setMethod($value);
            }elseif (method_exists($this, $appendMethod)) {
                $this->$appendMethod($value);
            }

        }

        return $this;
    }
}
