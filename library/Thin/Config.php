<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.16.
 */

namespace Thin;

use Thin\Application\ErrorException;

/**
 * Class Config
 */
class Config implements \Iterator, \Countable {
    /**
     * @var array|null
     */
    protected $data = null;
    /**
     * @var bool
     */
    protected $readOnly = false;
    /**
     * @var int
     */
    protected $position = 0;

    /**
     * Constructor
     *
     * @param array       $aData
     * @param string|null $aEnvironment
     * @param bool|false  $aReadOnly
     *
     * @throws ErrorException
     */
    public function __construct(array $aData = array(), $aEnvironment = null, $aReadOnly = false) {

        if (is_bool($aEnvironment)) {
            $aReadOnly    = $aEnvironment;
            $aEnvironment = null;
        }

        if (is_string($aEnvironment)) {
            if (array_key_exists($aEnvironment, $aData)) {
                $this->data = $aData[$aEnvironment];
            } else {
                throw new ErrorException(sprintf('The environment ("%s") is not exists.', $aEnvironment));
            }
        } else {
            $this->data = $aData;
        }

        $this->readOnly = (bool)$aReadOnly;
    }

    /**
     * Returns TRUE if this config is read only
     *
     * @return boolean
     */
    public function isReadOnly() {
        return $this->readOnly;
    }

    /**
     * Sets readonly attribute
     *
     * @param boolean $readOnly
     *
     * @return $this
     */
    public function setReadOnly($readOnly) {
        $this->readOnly = (bool)$readOnly;

        return $this;
    }

    /**
     * Returns a special variable value
     *
     * @param string     $aName
     * @param null|mixed $aDefault
     *
     * @return mixed
     */
    public function get($aName, $aDefault = null) {
        if (true === $this->has($aName)) {
            $aDefault = $this->data[$aName];
        }

        if (is_array($aDefault)) {
            $aDefault = new self($aDefault);
        }

        return $aDefault;
    }

    /**
     * Sets a special variable value if this collection is writable
     *
     * @param string     $aName
     * @param null|mixed $aValue
     *
     * @return $this
     * @throws ErrorException
     */
    public function set($aName, $aValue) {
        if (true === $this->readOnly) {
            throw new ErrorException('This data read only.');
        }

        $this->data[$aName] = $aValue;

        return $this;
    }

    /**
     * Returns TRUE if the special variable is exists
     *
     * @param string $aName
     *
     * @return bool
     */
    public function has($aName) {
        return array_key_exists($aName, $this->data);
    }

    /**
     * Returns this config as an array
     *
     * @return array
     */
    public function toArray() {
        return $this->data;
    }

    /**
     * Returns config item count
     *
     * @link http://php.net/manual/en/class.countable.php
     * @return int
     */
    public function count() {
        return count($this->data);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return void
     */
    public function rewind() {
        $this->position = 0;
    }

    /**
     * Returns the current element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return null|mixed|Config
     */
    public function current() {
        return $this->get($this->key());
    }

    /**
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return mixed
     */
    public function key() {
        $keys = array_keys($this->data);

        return $keys[$this->position];
    }

    /**
     * Move forward to next element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return void
     */
    public function next() {
        ++$this->position;
    }

    /**
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return bool
     */
    public function valid() {
        $keys = array_keys($this->data);

        return (isset($keys[$this->position]) and isset($this->data[$keys[$this->position]]));
    }

    /**
     * Creates a config from a file
     *
     * @param string $aFileName
     * @param string $aEnvironment
     *
     * @static
     * @return Config
     * @throws ErrorException
     */
    public static function create($aFileName, $aEnvironment = null) {
        if (!is_readable($aFileName)) {
            throw new ErrorException(sprintf('The file ("%s") is not exists or is not readable.', $aFileName));
        }

        $suffix = pathinfo($aFileName, PATHINFO_EXTENSION);

        switch (strtolower($suffix)) {
            case 'ini':
                $data = parse_ini_file($aFileName, true);

                foreach ($data as &$section) {
                    foreach ($section as $key => &$value) {
                        if (strpos($key, '.') !== false) {
                            $subKeys = explode('.', $key);
                            $lastKey = array_pop($subKeys);
                            $current = &$section;

                            foreach ($subKeys as $subKey) {
                                if (!isset($current[$subKey])) {
                                    $current[$subKey] = array();
                                }

                                $current = &$current[$subKey];
                            }

                            $current[$lastKey] = $value;

                            unset($section[$key]);
                        }
                    }
                }
                break;
            case 'json':
                $data = json_decode(file_get_contents($aFileName), true);
                break;
            case 'php':
                $data = include $aFileName;

                if (!is_array($data)) {
                    throw new ErrorException('The PHP file does not return array value');
                }
                break;
            default:
                throw new ErrorException(sprintf('Not supported file type ("%s").', $suffix));
        }

        return new self($data, $aEnvironment);
    }
}
