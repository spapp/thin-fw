<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.17.
 */

namespace Thin;

use Thin\Application\ErrorException;
use Thin\Http\Request;
use Thin\Http\Response;
use Thin\Traits\Configurable;
use Thin\Traits\ConfigurableInterface;

/**
 * Class Controller
 */
abstract class Controller implements ConfigurableInterface {

    use Configurable;

    const CONFIG_KEY = 'controller';
    const NS         = 'Controller';

    /**
     * @var Application
     */
    protected $application = null;

    /**
     * Constructor
     *
     * @param Application $aApplication
     */
    public function __construct(Application $aApplication) {
        $this->setApplication($aApplication);
    }

    /**
     * Returns application
     *
     * @return Application
     */
    final public function getApplication() {
        return $this->application;
    }

    /**
     * Sets current application
     *
     * @param Application $aApplication
     *
     * @return $this
     */
    final public function setApplication(Application $aApplication) {
        $this->application = $aApplication;

        return $this;
    }

    /**
     * Returns application response
     *
     * @return Response
     */
    final public function getResponse() {
        return $this->getApplication()->getResponse();
    }

    /**
     * Returns application request
     *
     * @return Request
     */
    final public function getRequest() {
        return $this->getApplication()->getRequest();
    }

    /**
     * Redirects the request
     *
     * Sends a redirection response and terminates the application
     *
     * @param string $aPath
     * @param int    $aStatusCode
     *
     * @return void
     * @throws \Exception
     */
    final public function redirect($aPath = '/', $aStatusCode = 307) {
        $location = $this->getRequest()->getBaseUrl() . $aPath;

        if ($aStatusCode < 300 or $aStatusCode > 399) {
            throw new ErrorException(sprintf('The %s status code is not part of the redirection class', $aStatusCode));
        }

        $this->getResponse()
             ->setStatusCode($aStatusCode)
             ->setHeader(Response::HEADER_LOCATION, $location)
             ->setBody('');

        $this->getApplication()->terminate(true);
    }

    /**
     * Forwards
     *
     * @param string $aControllerName
     * @param string $aMethodName
     * @param array  $aParams
     *
     * @return void
     */
    final public function forward($aControllerName, $aMethodName, array $aParams = array()) {
        $this->getApplication()->route($aControllerName, $aMethodName, $aParams);
    }

    /**
     * Create an instance of the Controller class.
     *
     * @param string      $aControllerName
     * @param Application $aApplication
     *
     * @static
     * @return Controller
     */
    public static function create($aControllerName, Application $aApplication) {
        $ns = $aApplication->getNs() . '\\' . self::NS . '\\';

        if (!preg_match('~^' . str_replace('\\', '\\\\', $ns) . '~', $aControllerName)) {
            $aControllerName = $ns . $aControllerName;
        }

        if (Autoloader::getInstance()->loadClass($aControllerName)) {
            return new $aControllerName($aApplication);
        }

        throw new ErrorException(sprintf("Class %s' not found", $aControllerName));
    }
}
