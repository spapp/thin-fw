<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.16.
 */

namespace Thin\Http;

use Thin\Template\Layout;

/**
 * Class Response
 */
class Response extends MessageAbstract {
    /**
     * @var Response
     * @static
     */
    protected static $instance = null;
    /**
     * @var array Response headers
     */
    protected $headers = array();
    /**
     * @var string Response message
     */
    protected $body = null;
    /**
     * @var int Response status code
     */
    protected $statusCode = 200;
    /**
     * @var bool
     */
    protected $isSent = false;
    /**
     * @var Layout
     */
    protected $layout = null;

    /**
     * Constructor
     */
    final protected function __construct() {
        $this->setHeader(self::HEADER_CONTENT_TYPE, self::MIME_HTML . '; charset=' . self::DEFAULT_CONTENT_CHARSET);
    }

    /**
     * Send the HTTP response
     *
     * @return boolean
     */
    public function send() {
        if (true === $this->isSent) {
            return false;
        }

        $this->isSent = true;
        $this->getContentType();

        switch ($this->getContentMime()) {
            case self::MIME_JSON:
                $body = json_encode($this->body);
                break;
            case self::MIME_FORM_URLENCODED:
                $body = http_build_query($this->body, null, '&', PHP_QUERY_RFC1738);
                break;
            case self::MIME_TEXT:
            case self::MIME_HTML:
            default:
                if (isset($this->layout)) {
                    $body =$this->getLayout()->setContent((string)$this->body)->render();
                } else {
                    $body = (string)$this->body;
                }
        }

        http_response_code($this->statusCode);

        foreach ($this->headers as $name => $value) {
            header($name . ': ' . $value, true);
        }

        if (!trim($body) and $this->statusCode >= 400) {
            $body = self::getMessage($this->statusCode);
        }

        echo $body;

        return true;
    }

    /**
     * Returns layout template
     *
     * @return Layout
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * Sets layout template
     *
     * @param Layout $aLayout
     *
     * @return $this
     */
    public function setLayout(Layout $aLayout) {
        $this->layout = $aLayout;

        return $this;
    }

    /**
     * Returns request Content-Type header value
     *
     * @return string
     */
    public function getContentType() {
        if (!isset($this->headers[self::HEADER_CONTENT_TYPE])) {
            $this->setHeader(self::HEADER_CONTENT_TYPE,
                             self::DEFAULT_CONTENT_MIME . '; ' . self::DEFAULT_CONTENT_CHARSET);
        }

        return $this->headers[self::HEADER_CONTENT_TYPE];
    }

    /**
     * Sets the response status code
     *
     * @param int $aStatusCode
     *
     * @return $this
     */
    public function setStatusCode($aStatusCode) {
        $this->statusCode = (int)$aStatusCode;

        return $this;
    }

    /**
     * Sets the response message
     *
     * @param mixed $aValue
     *
     * @return $this
     */
    public function setBody($aValue) {
        $this->body = $aValue;

        return $this;
    }

    /**
     * Appends value to response message
     *
     * @param mixed $aValue
     *
     * @return $this
     * @throws \Exception
     */
    public function appendBody($aValue) {
        if (is_string($this->body)) {
            $this->body .= $aValue;
        } elseif (is_array($this->body) and is_array($aValue)) {
            $this->body = array_merge($this->body, $aValue);
        } else {
            throw new \Exception('Type error');
        }

        return $this;
    }

    /**
     * Sets a response header
     *
     * @param string $aName
     * @param string $aValue
     *
     * @return $this
     */
    public function setHeader($aName, $aValue) {
        $name = str_replace(
                ' ',
                '-',
                ucwords(
                        str_replace(
                                array(
                                        '-',
                                        '_'
                                ),
                                ' ',
                                strtolower(trim($aName))
                        )
                )
        );

        $this->headers[$name] = $aValue;

        return $this;
    }

    /**
     * Returns the request instance
     *
     * @static
     * @return Response
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
