<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.16.
 */

namespace Thin\Http;

abstract class MessageAbstract {
    const SCHEME_HTTP  = 'http';
    const SCHEME_HTTPS = 'https';

    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_GET     = 'GET';
    const METHOD_HEAD    = 'HEAD';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_DELETE  = 'DELETE';
    const METHOD_TRACE   = 'TRACE';
    const METHOD_CONNECT = 'CONNECT';

    const MIME_TEXT            = 'text/plain';
    const MIME_HTML            = 'text/html';
    const MIME_JSON            = 'application/json';
    const MIME_FORM_URLENCODED = 'application/x-www-form-urlencoded';

    const HEADER_CONTENT_TYPE        = 'Content-Type';
    const HEADER_CONTENT_LENGTH      = 'Content-Length';
    const HEADER_CONTENT_DISPOSITION = 'Content-Disposition';
    const HEADER_HOST                = 'Host';
    const HEADER_USER_AGENT          = 'User-Agent';
    const HEADER_ACCEPT              = 'Accept';
    const HEADER_AUTHORIZATION       = 'Authorization';
    const HEADER_X_REQUESTED_WITH    = 'X-Requested-With';
    const HEADER_LOCATION            = 'Location';

    const DEFAULT_CONTENT_MIME    = self::MIME_FORM_URLENCODED;
    const DEFAULT_CONTENT_CHARSET = 'utf-8';

    const DEFAULT_HTTP_PORT  = 80;
    const DEFAULT_HTTPS_PORT = 443;

    protected static $messages = array(
        //Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
        //Successful 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            226 => 'IM Used',
        //Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
        //Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            418 => 'I\'m a teapot',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            426 => 'Upgrade Required',
            428 => 'Precondition Required',
            429 => 'Too Many Requests',
            431 => 'Request Header Fields Too Large',
        //Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            510 => 'Not Extended',
            511 => 'Network Authentication Required'
    );

    /**
     * Returns request Content-Type header charset part
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getContentCharset($aDefault = self::DEFAULT_CONTENT_CHARSET) {
        $contentType = explode(';', $this->getContentType());

        if (isset($contentType[1])) {
            $aDefault = $contentType[1];
        }

        return $aDefault;
    }

    /**
     * Returns request Content-Type header mime type part
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getContentMime($aDefault = self::DEFAULT_CONTENT_MIME) {
        $contentType = explode(';', $this->getContentType());

        if (isset($contentType[0])) {
            $aDefault = $contentType[0];
        }

        return $aDefault;
    }

    /**
     * Returns a text representation of HTTP response code
     *
     * @param int $aStatusCode
     *
     * @static
     * @return string
     */
    public static function getMessage($aStatusCode) {
        if (isset(self::$messages[$aStatusCode])) {
            return self::$messages[$aStatusCode];
        }

        return '';
    }

    /**
     * Returns request Content-Type header value
     *
     * @return string
     */
    abstract public function getContentType();
}
