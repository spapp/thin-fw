<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.16.
 */

namespace Thin\Http;

use Thin\Config;

/**
 * HTTP Request Class
 */
class Request extends MessageAbstract {
    const VARIABLE_SERVER           = '_SERVER';
    const VARIABLE_HTTP_GET         = '_GET';
    const VARIABLE_HTTP_POST        = '_POST';
    const VARIABLE_HTTP_FILE_UPLOAD = '_FILES';
    const VARIABLE_ENV              = '_ENV';
    /**
     * @var Request
     * @static
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $predefinedVariables = array();
    /**
     * @var array
     */
    protected $params = null;

    /**
     * Constructor
     */
    final protected function __construct() {

    }

    /**
     * Returns server and execution environment information
     *
     * @return Config
     */
    public function getServer() {
        return $this->getPredefinedVariable(self::VARIABLE_SERVER, true);
    }

    /**
     * Returns HTTP GET variables
     *
     * @return Config
     */
    public function getGet() {
        return $this->getPredefinedVariable(self::VARIABLE_HTTP_GET, true);
    }

    /**
     * Returns HTTP POST variables
     *
     * @return Config
     */
    public function getPost() {
        return $this->getPredefinedVariable(self::VARIABLE_HTTP_POST, true);
    }

    /**
     * Returns HTTP File Upload variables
     *
     * @return Config
     */
    public function getFiles() {
        return $this->getPredefinedVariable(self::VARIABLE_HTTP_FILE_UPLOAD, true);
    }

    /**
     * Returns environment variables
     *
     * @return Config
     */
    public function getEnv() {
        return $this->getPredefinedVariable(self::VARIABLE_ENV, true);
    }

    /**
     * Returns HTTP request uri path
     *
     * @return string
     */
    public function getPath() {
        $path = parse_url($this->getServer()->get('REQUEST_URI', '/'), PHP_URL_PATH);

        return trim($path);
    }

    /**
     * Returns request scheme (http or https)
     *
     * @return string
     */
    public function getScheme() {
        switch (true) {
            case (self::SCHEME_HTTPS === $this->getServer()->get('REQUEST_SCHEME')):
            case ('on' === $this->getServer()->get('HTTPS')):
            case (self::DEFAULT_HTTPS_PORT === $this->getPort()):
                $scheme = self::SCHEME_HTTPS;
                break;
            default:
                $scheme = self::SCHEME_HTTP;
        }

        return $scheme;
    }

    /**
     * Returns request host name
     *
     * @return string
     */
    public function getHost() {
        $host = $this->getServer()->get('HTTP_HOST');

        if (!$host) {
            $host = $this->getServer()->get('SERVER_NAME');
        }

        return $host;
    }

    /**
     * Returns request port number
     *
     * @return int
     */
    public function getPort() {
        return (int)$this->getServer()->get('SERVER_PORT');
    }

    /**
     * Returns request base url
     *
     * @example http://example.com
     * @example https://example.com
     * @example http://example.com:8080
     *
     * @return string
     */
    public function getBaseUrl() {
        $port = $this->getPort();

        if (self::DEFAULT_HTTPS_PORT === $port or self::DEFAULT_HTTP_PORT === $port) {
            $port = 0;
        }

        return sprintf(
                "%s://%s%s",
                $this->getScheme(),
                $this->getHost(),
                ($port > 0 ? ':' . $port : '')
        );
    }

    /**
     * Returns the request method
     *
     * @return string
     */
    public function getMethod() {
        return $this->getServer()->get('REQUEST_METHOD');
    }

    /**
     * Returns a request header value
     *
     * @param string $aName
     * @param string $aDefault
     *
     * @return string
     */
    public function getHeader($aName, $aDefault) {
        $name = strtoupper(str_replace('-', '_', $aName));

        if (!in_array($name,
                      array(
                              'CONTENT_TYPE',
                              'CONTENT_LENGTH'
                      ))
        ) {
            $name = 'HTTP_' . $name;
        }

        return $this->getServer()->get($name, $aDefault);
    }

    /**
     * Returns request params variables
     *
     * @return Config
     */
    public function getParams() {
        if (null === $this->params) {
            switch ($this->getMethod()) {
                case self::METHOD_GET:
                    return $this->getGet();
                    break;
                case self::METHOD_POST:
                case self::METHOD_PUT:
                case self::METHOD_DELETE:
                    $params = $this->getParamsFromRawBody();

                    if ($params) {
                        $this->params = new Config($params);
                    }

                    break;
            }
        }

        return $this->params;
    }

    /**
     * Returns request Content-Type header value
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getContentType($aDefault = '') {
        return $this->getHeader(self::HEADER_CONTENT_TYPE, $aDefault);
    }

    /**
     * Returns request Accept header value
     *
     * @param string $aDefault
     *
     * @return string
     */
    public function getAccept($aDefault = '') {
        return array_shift(explode(',', $this->getHeader(self::HEADER_ACCEPT, $aDefault)));
    }

    /**
     * Returns HTTP request raw body value
     *
     * @return string
     */
    public function getRawBody() {
        return file_get_contents('php://input');
    }

    /**
     * Returns the request instance
     *
     * @static
     * @return Request
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns parsed HTTP request raw body
     *
     * @return array|null
     */
    private function getParamsFromRawBody() {
        switch ($this->getContentMime()) {
            case self::MIME_JSON:
                $params = json_decode($this->getRawBody(), true);
                break;
            case self::MIME_FORM_URLENCODED:
                parse_str($this->getRawBody(), $params);
                break;
            default:
                $params = null;
        }

        return $params;
    }

    /**
     * Returns a server predefined variable collection
     *
     * @param string $aName
     * @param bool   $aReadOnly
     *
     * @return Variable
     */
    private function getPredefinedVariable($aName, $aReadOnly) {
        if (!array_key_exists($aName, $this->predefinedVariables)) {
            switch ($aName) {
                case self::VARIABLE_SERVER:
                    $data = $_SERVER;
                    break;
                case self::VARIABLE_HTTP_GET:
                    $data = $_GET;
                    break;
                case self::VARIABLE_HTTP_POST:
                    $data = $_POST;
                    break;
                case self::VARIABLE_HTTP_FILE_UPLOAD:
                    $data = $_FILES;
                    break;
                case self::VARIABLE_ENV:
                    $data = $_ENV;
                    break;
            }

            $this->predefinedVariables[$aName] = new Config($data, $aReadOnly);
        }

        return $this->predefinedVariables[$aName];
    }
}
