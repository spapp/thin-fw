<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   Thin
 * @since     2015.06.25.
 */

namespace Thin\Application;

/**
 * Class ErrorException
 */
class ErrorException extends \ErrorException {
    protected static $htmlTpl = <<<HTMLTPL
<h1>%s (%s)</h1>
<h3>%s</h3>
<pre>
<strong>File:</strong>\n%s\n
<strong>Line:</strong>\n%s\n
<strong>Stack trace:</strong>\n%s
</pre>
HTMLTPL;

    /**
     * Returns error data as a HTML string
     *
     * @see $htmlTpl
     * @return string
     */
    public function asHtml() {
        return sprintf(
                self::$htmlTpl,
                get_class($this),
                $this->getCode(),
                $this->getMessage(),
                $this->getFile(),
                $this->getLine(),
                $this->getTraceAsString()
        );
    }
}